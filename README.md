# Coop Resources


WIP;

- [Hacking Capitalism](http://wiki.p2pfoundation.net/Hacking_Capitalism)
- [Jim Brown, TED Talk, How to stop poverty: start a worker-owned cooperative](https://www.youtube.com/watch?v=LFyl0zz2yqs)
- [Platform Cooperativism](http://platformcoop.net/), [resources](http://platformcoop.net/resources)

> All over the world, individuals, businesses, and cities are involved in the platform cooperativism movement.
There are many ways to get involved: from joining a local group to  starting your own platform co-op. 


## Tech Coops

- [techworker.coop](https://www.techworker.coop/)
 
> We are worker cooperatives (businesses owned and democratically controlled by our workers)
offering a wide range of media, communications, and computer technology goods and services.

- [A Technology Freelancer's guide to starting a Worker Cooperative](https://techworker.coop/sites/default/files/TechCoopHOWTO.pdf)
 

- [Cooperative Technologists : coops.tech](https://www.coops.tech/)
 
> Building a tech industry that's better for its workers and customers through co-operation, democracy and worker ownership.

- [Tech Collective](https://techcollective.com/#welcome)

> TechCollective is a worker-owned cooperative offering enterprise-grade technical support to small businesses & non-profits. 
Our teams in San Francisco & Boston work with hundreds of such clients every year. 
We would love to do the same for your organization! Mac or PC, local or cloud, we've got you covered.


## Case Studies


[*list of worker cooperatives*](https://en.wikipedia.org/wiki/List_of_worker_cooperatives)

- [Mondragon Cooperative Corporation](https://en.wikipedia.org/wiki/Mondragon_Corporation)
    - [Understanding the Mondragon Worker Cooperative Corporation](https://www.youtube.com/watch?v=8bcNfbGxAdY)
    - [Employee Ownership as Strategy](https://www.youtube.com/watch?v=zijMTOz9eYg)
    
## Books

- [Our to Hack and to Own](pdf/Schneider_Scholz.pdf)
- [Hacking Capitalism](http://downloads.gvsig.org/download/people/vagazzi/Hacking%20Capitalism.pdf)